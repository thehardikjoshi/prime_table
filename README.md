# PrimeTable


## Objective
- Write a program that prints out a multiplication table of the first N prime numbers.
- The program must run from the command line and print a table to STDOUT.
- The first row and column of the table should have the N primes, with each cell containing the product of the primes for the corresponding row and column.
- Allow the user to specify different table sizes through a command line option. If the option is not used, the table should contain the first 10 primes by default.

## Usage

    bin/prime_table         # default 10
    
or
    
    bin/prime_table 5       # size of table

## Complexity 

Generating primes: O(n^2)

Rendering table: O(n^2)

## How fast does your code run? How does it scale? 
As 'n' gets bigger the program is going to start slowing down. For smaller 'n' number of primes this solution will suffice.
