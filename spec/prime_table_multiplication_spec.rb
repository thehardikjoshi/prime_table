require 'spec_helper'

RSpec.describe PrimeTable::Multiplication do
  context "defaults and setters" do
    let(:table) { PrimeTable::Multiplication.new() }

    it "has a blank buffer attribute" do
      expect(table.buffer).to eq('')
    end

    it "has a default limit 10" do
      expect(table.limit).to eq(10)
    end
    
    it "has a limit attribute" do
      table = PrimeTable::Multiplication.new(5)
      expect(table.limit).to eq(5)
    end
  end

  context "operations" do 
    let(:table) { PrimeTable::Multiplication.new(5) }

    describe "#find_primes" do
      it "returns prime numbers upto given max" do
        expect(table.find_primes).to match_array([2,3,5,7,11])
      end
    end

    describe "#calculate" do
      it "creates buffer of multiplication table of derived n primes" do
        table.limit = 2
        table.calculate
        expect(table.buffer).to eq("      2    3   \n\n2  |  4    6   \n\n3  |  6    9   \n\n")
      end
    end
  end
end
