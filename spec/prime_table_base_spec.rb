require 'spec_helper'

RSpec.describe PrimeTable::Base do
  let(:table) { PrimeTable::Base.new(10) }

  describe "#find_primes" do
    it "returns prime numbers upto given max" do
      expect{table.calculate}.to raise_error(NotImplementedError)
    end
  end
end
