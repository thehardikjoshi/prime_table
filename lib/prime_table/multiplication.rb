module PrimeTable
  class Multiplication < Base
    OPERATOR = "*"

    def calculate
      rows = find_primes
      columns = rows

      self.buffer = "     "
      columns.each {|column_num| self.buffer += " %-3d " % column_num}
      self.buffer += "\n\n"

      rows.each do |row_num|
        self.buffer += "%-3d| " % row_num
        columns.each {|column_num| self.buffer += " %-3d " % (column_num.send(OPERATOR, row_num))}
        self.buffer += "\n\n"
      end
      self.buffer
    end
  end
end
