module PrimeTable
  class Base
    attr_accessor :limit, :buffer

    def initialize(limit=10)
      @limit = limit
      @buffer = ''
    end

    def find_primes
      primes = []

      if limit >= 2
        primes[0] = 2
        self.limit = self.limit - 1
      end
      
      counter = 0
      num = 3
      while counter < self.limit do
        
        is_prime = true
        if num % 2 == 0
          is_prime = false
        else
          3.step(num/2, 2) do |stepper_num|
            if num % stepper_num == 0
              is_prime = false
              break
            end
          end
        end

        if is_prime
          counter += 1
          primes << num
        end
        num += 1
      end
      
      primes
    end

    def calculate
      raise NotImplementedError
    end
  end


end
